package interview;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class Tokenizer {
	
		public static void main(String[] args) {
			try {
				File file = new File("/Users/donaldroyer/Desktop/log");
				FileReader fileReader = new FileReader(file);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				StringBuffer stringBuffer = new StringBuffer();
				String line;
				HashMap map = new HashMap();
				HashMap countermap = new HashMap();
				while ((line = bufferedReader.readLine()) != null) {
					int startpos = line.indexOf("REQ_");
					int endpos = line.indexOf("uid-");
					
					if (endpos > 2) {
					
						String reqID = line.substring(startpos, endpos);
						if (map.containsKey(reqID))
						{
							String value = (String)map.get(reqID);
							value+= line;
							map.put(reqID, value);
							int counter = ((Integer)countermap.get(reqID)).intValue();
							counter +=1;

							countermap.put(reqID, Integer.valueOf(counter));	
							
						}
						else{
							map.put(reqID, line);
							countermap.put(reqID, new Integer("1"));
						}
					}
					
				}
				fileReader.close();
				
				PrintStream ps = new PrintStream("/Users/donaldroyer/Desktop/file.txt");
				PrintStream orig = System.out;
				System.setOut(ps);
				
			
				System.out.println("Requests: " + map.size());
				/*
				Iterator it = map.entrySet().iterator();
			    while (it.hasNext()) {
			        Map.Entry pair = (Map.Entry)it.next();
			        System.out.println(pair.getKey() + " = " + pair.getValue());
			    }
				*/
				
				Iterator it = countermap.entrySet().iterator();
			    while (it.hasNext()) {
			        Map.Entry pair = (Map.Entry)it.next();
			        System.out.println(pair.getKey() + " = " + pair.getValue());
			    }
			    
			  System.setOut(orig);
				ps.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}